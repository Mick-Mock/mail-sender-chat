const schedule = require('node-schedule'),
  mail = require('./mail');

exports.initAutoMailing = function (settings) {

  var rule0 = new schedule.RecurrenceRule();
  rule0.dayOfWeek = [new schedule.Range(1, 5)];
  rule0.minute = 0;

  var a = schedule.scheduleJob(rule0, function () {
    console.log('00');
    mail.mailSender();
  });

  var rule1 = new schedule.RecurrenceRule();
  rule1.dayOfWeek = [new schedule.Range(1, 5)];
  rule1.minute = 15;

  var b = schedule.scheduleJob(rule1, function () {
    console.log('15');
    mail.mailSender();
  });

  var rule2 = new schedule.RecurrenceRule();
  rule2.dayOfWeek = [new schedule.Range(1, 5)];
  rule2.minute = 30;

  var c = schedule.scheduleJob(rule2, function () {
    console.log('30');
    mail.mailSender();
    mail.mailSender(true);
  });

  var rule3 = new schedule.RecurrenceRule();
  rule3.dayOfWeek = [new schedule.Range(1, 5)];
  rule3.minute = 45;

  var d = schedule.scheduleJob(rule3, function () {
    console.log('45');
    mail.mailSender();
  });

};