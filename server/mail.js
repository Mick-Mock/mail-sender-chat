'use strict';
const nodemailer = require('nodemailer'),
  Handler = require('../server/handler'),
  success = Handler.respondWithResult,
  failure = Handler.respondWithError;

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'mm.mick.mack.mock@gmail.com',
    pass: 'qwertyqwerty'
  }
});

var froms = [
  'Mari Jean',
  'Peter Pope',
  'Henry Larson',
  'Myriame Dupont',
  'Jean Thy',
  'Jean Nemard',
  'Thierry Gollo',
  'Richard Donnet',
  'Serena WanDerwoodsen',
  'sheldon Cooper',
  'Blaire Waldorf',
  'Peper Pots',
  'Olivia Pope',
  'Thomas Tognion',
  'Agathe Zepower',
  'Jacque Ouzzi',
  'Lara Tatouille',
  'Yves Remords',
  'Pascal Obistro',
  'Harry Cover',
  'Mohamed Lee',
  'Eric Luncher',
  'Jeanne Doe',
  'Sophie Phonfec',
  'Leonard Hofstadter',
  'Raj Koutrapali',
  'Amy Farrah Fowler',
  'Perceval LeGalois',
  'Aline Dubois',
  'Amal Hodant',
  'Janine Lefevre',
  'Josephine Garnier',
  'Carla Rousel',
  'Fleur Gauthier',
  'Martin Nicolas',
  'Odette Roux'
];

var name = [
  'EMMA',
  'LÉNA',
  'CAMILLE',
  'LOUISE',
  'CHLOÉ',
  'MANON',
  'ALICE',
  'JADE',
  'LÉA',
  'ZOÉ',
  'MILA',
  'JULIA',
  'INÈS',
  'LOLA',
  'EDEN',
  'JULIETTE',
  'AMBRE',
  'AGATHE',
  'ÉLÉNA',
  'LUCIE',
  'ÉLISE',
  'MARGAUX',
  'LÉANA',
  'LÉONIE',
  'ROSE',
  'MIA',
  'CLÉMENCE',
  'CLARA',
  'ANNA',
  'CAPUCINE',
  'ROMANE',
  'EVA',
  'JEANNE',
  'LOU',
  'MAËLYS',
  'CHARLOTTE',
  'SOLINE',
  'CANDICE',
  'LOUNA',
  'ELSA',
  'NOÉMIE',
  'ALICIA',
  'GIULIA',
  'MATHILDE',
  'NINA',
  'ANDRÉA',
  'EMY',
  'JUSTINE',
  'LANA',
  'LÉANE'
];

var surname = [
  'MARTIN',
  'BERNARD',
  'DBOIS',
  'THOMAS',
  'RICHARD',
  'PETI',
  'DURANT',
  'LEFEBVRE',
  'GARCIA',
  'ROUX',
  'MULLER',
  'PERRIER',
  'FAURE',
  'MORIN',
  'MASSON',
  'DUVAL',
  'JOLI',
  'GARCIA',
];

var mailOptions = {
  from: '"Mari Jean" <mm.mick.mack.mock@gmail.com>', // sender address
  to: /*'m.peyre@outlook.com',//*/'serviceclient@sotrendoo.com', // chat
  subject: 'Petite question.', // Subject line
  text: ' Bonjour,\n La box été est elle toujours disponible ? ', // plain text body
};

let mailNuxe = {
  from: '"Mari Jean" <mm.mick.mack.mock@gmail.com>', // sender address
  to: /*'m.peyre@outlook.com',//*/'sav@nuxe.com', // chat
  subject: 'Petit soucis.', // Subject line
  text: ' Bonjour,\n Je n\'arrive pas a declarer mes codes privilege ', // plain text body
};

exports.sendMailFormApi = function (req, res) {
  var from = req.body.from;
  var to = req.body.to;
  var subject = req.body.subject;
  var text = req.body.text;

  return formSender(from, to, subject, text)
    .then(success(res))
    .catch(failure(res));
};

var formSender = function (from, to, subject, text) {
  return new Promise(function (resolve, reject) {

    var options = { from: '"' + from + '" <mm.mick.mack.mock@gmail.com>', to, subject, text };

    console.log(JSON.stringify(options, 1, 1));

    transporter.sendMail(options)
      .then(resolve, reject);

    transporter.close();
  });
};

exports.sendMailApi = function (req, res) {
  var qty = 1;
  if (req.params.qty) qty = req.params.qty;
  var tasks = [];
  for (let i = 0; i < qty; i++) {
    tasks.push(mailSender());
  }
  return Promise.all(tasks)
    .then(success(res))
    .catch(failure(res));
};

var mailSender = function (nuxe = false) {
  return new Promise(function (resolve, reject) {

    let r = Math.random();
    if (r < (1 / 3)) {
      let i = Math.floor(Math.random() * froms.length);
      mailOptions.from = '"' + froms[i] + '" <mm.mick.mack.mock@gmail.com>';
    }
    else {
      let j = Math.floor(Math.random() * name.length);
      let k = Math.floor(Math.random() * surname.length);
      mailOptions.from = '"' + name[j] + ' ' + surname[k] + '" <mm.mick.mack.mock@gmail.com>';
    }



    if (nuxe) {
      mailOptions.subject = mailNuxe.subject;
      mailOptions.text = mailNuxe.text;
    }

    console.log(mailOptions.from);

    transporter.sendMail(mailOptions)
      .then(resolve, reject);

    transporter.close();
  });
};

exports.mailSender = mailSender;