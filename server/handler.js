"use strict";


function getSuccessJson(response) {
  return {
    message: 'Successful message.',
    content: response
  };
}


function getErrorJson(error) {
  return {
    message: 'Error message.',
    error: error
  };
}


var respondWithResult = function(res, response, statusCode=200) {

  if (response) {
    return res.status(statusCode).json(getSuccessJson(response));
  } else {
    return function(response) {
      return res.status(statusCode).json(getSuccessJson(response));
    };
  }
};
exports.respondWithResult = respondWithResult;


var respondWithError = function(res, error, statusCode=500) {

  if (error) {
    error = (error && error.stack) ? error.stack : error;
    console.error('handleError:', JSON.stringify(error, 1, 1));
    res.status(statusCode).send(getErrorJson(error));
  } else {
    return function(error) {
      error = (error && error.stack) ? error.stack : error;
      console.error('handleError:', JSON.stringify(error, 1, 1));
      res.status(statusCode).send(getErrorJson(error));
    };
  }
};
exports.respondWithError = respondWithError;


function rejectWithLog (aFunction, reject) {
  return function(err) {
    console.log('error in ' + aFunction.name + '(): ', err);
    return reject(err);
  };
}
exports.rejectWithLog = rejectWithLog;