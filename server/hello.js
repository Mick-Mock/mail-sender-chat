'use strict';
const Handler = require('../server/handler'),
  success = Handler.respondWithResult,
  failure = Handler.respondWithError;

  exports.helloApi = function (req, res) {
    return hello()
      .then(success(res))
      .catch(failure(res));
  };

  var hello = function(){
    return new Promise(function (resolve, reject) {
      console.log('I\'m still alive !!!');
      return resolve('still alive');

    });
  }