var express = require('express');
var router = express.Router();
var hello = require("../server/hello");

router.get('/', hello.helloApi);

module.exports = router;
