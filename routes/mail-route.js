var express = require('express');
var router = express.Router();
var mail = require("../server/mail");

router.get('/mail/:qty', mail.sendMailApi);
router.post('/mail', mail.sendMailFormApi);

module.exports = router;
