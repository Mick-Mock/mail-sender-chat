'use strict';

module.exports = function(app) {
	app.use('/hello', require('./hello-route'));
	app.use('/mails',  require('./mail-route'));
};
