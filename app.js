const express = require('express');
const bodyParser = require('body-parser');
const schedule = require('./server/schedule');


const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var routes = require('./routes/index.js');
routes(app);

var server = app.listen(8080, function () {
  console.log('allive on port 8080');

  schedule.initAutoMailing();

});